package com.epam.model;
import java.util.Objects;
/**
 * Class describes all elements in XML.
 * Created by Borys Latyk on 14/12/2019.
 *
 * @version 2.1
 * @since 14.12.2019
 */
public class Plane {
    private int planeN;
    private String model;
    private String origin;
    private int price;
    private String type;
    private Integer cruew;
    private int combatkit;
    private int maxspeed;
    private boolean stealthtechnology;
    private double length;
    private double width;
    private double height;

    public Plane() {

    }

    public Plane(int planeN, String model, String origin, int price, String type,
                 Integer cruew, int combatkit, int maxspeed,
                 boolean stealthtechnology, double length, double width, double height) {
        this.planeN = planeN;
        this.model = model;
        this.origin = origin;
        this.price = price;
        this.type = type;
        this.cruew = cruew;
        this.combatkit = combatkit;
        this.maxspeed = maxspeed;
        this.stealthtechnology = stealthtechnology;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public int getPlaneN() {
        return planeN;
    }

    public String getModel() {
        return model;
    }

    public String getOrigin() {
        return origin;
    }

    public int getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public Integer getCruew() {
        return cruew;
    }

    public int getCombatkit() {
        return combatkit;
    }

    public int getMaxspeed() {
        return maxspeed;
    }

    public boolean isStealthtechnology() {
        return stealthtechnology;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setPlaneN(int planeN) {
        this.planeN = planeN;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCruew(Integer cruew) {
        this.cruew = cruew;
    }

    public void setCombatkit(int combatkit) {
        this.combatkit = combatkit;
    }

    public void setMaxspeed(int maxspeed) {
        this.maxspeed = maxspeed;
    }

    public void setStealthtechnology(boolean stealthtechnology) {
        this.stealthtechnology = stealthtechnology;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "planeN=" + planeN +
                ", model='" + model + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", cruew=" + cruew +
                ", combatkit=" + combatkit +
                ", maxspeed=" + maxspeed +
                ", stealthtechnology=" + stealthtechnology +
                ", length=" + length +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plane)) return false;
        Plane plane = (Plane) o;
        return price == plane.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }
}
