package com.epam.model.parsers;

import com.epam.Application;
import com.epam.model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class shows a example of Stax parser.
 * Created by Borys Latyk on 14/12/2019.
 *
 * @version 2.1
 * @since 14.12.2019
 */
public class StaxExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void buildStax() {
        String fileName = "E:/Epam knowledge/myxml/src/main/resources/xml/planesXML.xml";
        List<Plane> planeList2 = parseXML(fileName);
        Comparator<Plane> comparator3=((o1, o2) -> o2.getMaxspeed()-o1.getMaxspeed());
        Collections.sort(planeList2,comparator3);
        for (Plane p : planeList2) {
            logger1.info(p.toString());
        }

    }

    private static List<Plane> parseXML(String fileName) {
        List<Plane> planeList3 = new ArrayList<>();
        Plane plane = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("plane")) {
                        plane = new Plane();
                        //Get the 'id' attribute from Plane element
                        Attribute idAttr = startElement.getAttributeByName(new QName("planeN"));
                        if (idAttr != null) {
                            plane.setPlaneN(Integer.parseInt(idAttr.getValue()));
                        }
                    }
                    //set the other varibles from xml elements
                    else if (startElement.getName().getLocalPart().equals("model")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setModel(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("origin")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setOrigin(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("price")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("type")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setType(xmlEvent.asCharacters().getData());
                    }else if (startElement.getName().getLocalPart().equals("cruew")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setCruew(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("combatkit")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setCombatkit(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("maxspeed")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setMaxspeed(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("stealthtechnology")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setStealthtechnology(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("length")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("width")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setWidth(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }else if (startElement.getName().getLocalPart().equals("height")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        plane.setHeight(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }
                }
                //if Plane end element is reached, add plane object to list
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("plane")) {
                        planeList3.add(plane);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return planeList3;

    }
}