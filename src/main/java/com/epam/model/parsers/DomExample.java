package com.epam.model.parsers;

import com.epam.Application;
import com.epam.model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * Class shows a example of DOM parser.
 * Created by Borys Latyk on 14/12/2019.
 *
 * @version 2.1
 * @since 14.12.2019
 */
public class DomExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void buildDom() throws ParserConfigurationException, IOException, SAXException {
        List<Plane> aircrafts = new ArrayList<>();
        Plane plane;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse("E:/Epam knowledge/myxml/src/main/resources/xml/planesXML.xml");
        document.getDocumentElement().normalize();
        NodeList planesElements = document.getElementsByTagName("plane");

        for (int i = 0; i < planesElements.getLength(); i++) {
            Node node = planesElements.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                plane = new Plane();
                plane.setPlaneN(Integer.parseInt(eElement.getAttribute("planeN")));
                plane.setModel(eElement.getElementsByTagName("model").item(0).getTextContent());
                plane.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                plane.setPrice(Integer.parseInt(eElement.getElementsByTagName("price").item(0).getTextContent()));
                plane.setType(eElement.getElementsByTagName("type").item(0).getTextContent());
                plane.setCruew(Integer.parseInt(eElement.getElementsByTagName("cruew").item(0).getTextContent()));
                plane.setCombatkit(Integer.parseInt(eElement.getElementsByTagName("combatkit").item(0).getTextContent()));
                plane.setMaxspeed(Integer.parseInt(eElement.getElementsByTagName("maxspeed").item(0).getTextContent()));
                plane.setStealthtechnology(Boolean.parseBoolean(eElement.getElementsByTagName
                        ("stealthtechnology").item(0).getTextContent()));
                plane.setLength(Double.parseDouble(eElement.getElementsByTagName("length").item(0).getTextContent()));
                plane.setWidth(Double.parseDouble(eElement.getElementsByTagName("width").item(0).getTextContent()));
                plane.setHeight(Double.parseDouble(eElement.getElementsByTagName("height").item(0).getTextContent()));

                aircrafts.add(plane);
            }
        }
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Plane plane1 = (Plane) o1;
                Plane plane2 = (Plane) o2;
                if (plane1.getPrice() < plane2.getPrice()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
        Collections.sort(aircrafts, comparator);

        for (Plane p : aircrafts) {
            logger1.info(p);
        }
    }
}
