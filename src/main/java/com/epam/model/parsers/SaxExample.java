package com.epam.model.parsers;

import com.epam.Application;
import com.epam.model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class shows a example of SAX parser.
 * Created by Borys Latyk on 14/12/2019.
 *
 * @version 2.1
 * @since 14.12.2019
 */
public class SaxExample extends DefaultHandler {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    // List to hold Planes object
    private List<Plane> planeList = null;
    private Plane plane = null;
    private StringBuilder data = null;

    // getter method for plane list
    public List<Plane> getPlaneList() {
        return planeList;
    }

    boolean bModel = false;
    boolean bOrigin = false;
    boolean bPrice = false;
    boolean bType = false;
    boolean bCruew = false;
    boolean bCombatkit = false;
    boolean bMaxspeed = false;
    boolean bStealthtechnology = false;
    boolean bLength = false;
    boolean bWidth = false;
    boolean bHeight = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        if (qName.equalsIgnoreCase("plane")) {
            // create a new Plane and put it in Map
            String id = attributes.getValue("planeN");
            // initialize Plane object and set id attribute
            plane = new Plane();
            plane.setPlaneN(Integer.parseInt(id));
            // initialize list
            if (planeList == null)
                planeList = new ArrayList<>();
        } else if (qName.equalsIgnoreCase("model")) {
            // set boolean values for fields, will be used in setting Plane variables
            bModel = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase("price")) {
            bPrice = true;
        } else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("cruew")) {
            bCruew = true;
        } else if (qName.equalsIgnoreCase("combatkit")) {
            bCombatkit = true;
        } else if (qName.equalsIgnoreCase("maxspeed")) {
            bMaxspeed = true;
        } else if (qName.equalsIgnoreCase("stealthtechnology")) {
            bStealthtechnology = true;
        } else if (qName.equalsIgnoreCase("length")) {
            bLength = true;
        } else if (qName.equalsIgnoreCase("width")) {
            bWidth = true;
        } else if (qName.equalsIgnoreCase("height")) {
            bHeight = true;
        }

        // create the data container
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (bModel) {
            plane.setModel(data.toString());
            bModel = false;
        } else if (bOrigin) {
            plane.setOrigin(data.toString());
            bOrigin = false;
        } else if (bPrice) {
            plane.setPrice(Integer.parseInt(data.toString()));
            bPrice = false;
        } else if (bType) {
            plane.setType(data.toString());
            bType = false;
        } else if (bCruew) {
            plane.setCruew(Integer.parseInt(data.toString()));
            bCruew = false;
        } else if (bCombatkit) {
            plane.setCombatkit(Integer.parseInt(data.toString()));
            bCombatkit = false;
        } else if (bMaxspeed) {
            plane.setMaxspeed(Integer.parseInt(data.toString()));
            bMaxspeed = false;
        } else if (bStealthtechnology) {
            plane.setStealthtechnology(Boolean.parseBoolean(data.toString()));
            bStealthtechnology = false;
        } else if (bLength) {
            plane.setLength(Double.parseDouble(data.toString()));
            bLength = false;
        } else if (bWidth) {
            plane.setWidth(Double.parseDouble(data.toString()));
            bWidth = false;
        } else if (bHeight) {
            plane.setHeight(Double.parseDouble(data.toString()));
            bHeight = false;
        }

        if (qName.equalsIgnoreCase("plane")) {
            // add Plane object to list
            planeList.add(plane);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        data.append(new String(ch, start, length));
    }

    public void buildSax() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxExample handler = new SaxExample();
            saxParser.parse("E:/Epam knowledge/myxml/src/main/resources/xml/planesXML.xml", handler);
            //Get Planes list
            List<Plane> planeList = handler.getPlaneList();
            //print planes information
            Comparator<Plane> comparator2 = ((o1, o2) -> o2.getCombatkit() - o1.getCombatkit());
            Collections.sort(planeList, comparator2);

            for (Plane p : planeList)
                logger1.info(p);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}



