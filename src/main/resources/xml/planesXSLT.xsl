<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>War Aircrafts</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Model</th>
                        <th>Origin</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Cruew</th>
                        <th>Combat kit</th>
                        <th>Stealth technology</th>
                        <th>Length</th>
                        <th>Width</th>
                        <th>Height</th>
                    </tr> <xsl:for-each select="Planes/plane"><tr>
                    <td><xsl:value-of select="model"/></td>
                    <td><xsl:value-of select="origin"/></td>
                    <td><xsl:value-of select="price"/></td>
                    <td><xsl:value-of select="type"/></td>
                    <td><xsl:value-of select="cruew"/></td>
                    <td><xsl:value-of select="combatkit"/></td>
                    <td><xsl:value-of select="stealthtechnology"/></td>
                    <td><xsl:value-of select="length"/></td>
                    <td><xsl:value-of select="width"/></td>
                    <td><xsl:value-of select="height"/></td>
                </tr> </xsl:for-each>
                </table> </body>
        </html>
    </xsl:template>
</xsl:stylesheet>